<?php


# FRONT CONTROLLER

#1 общие настройки
ini_set('Display errors', 1);
error_reporting(E_ALL);

session_start();

#2 Подключение файловсистемы
define('ROOT', dirname(__FILE__));
//Атозагрузка классов
require_once(ROOT.'/components/Autoload.php');

/*
за счет использования функции автозагрузки классов 
// больше нне требуется делать подключение в ручную
//require_once(ROOT.'/components/Router.php');
//require_once(ROOT.'/components/Db.php'); 
 #3 Подключение БД
 */


#4 вызов Router
$router = new Router();
$router->run();