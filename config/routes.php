<?php

return array(
    
    'product/([0-9]+)' => 'product/view/$1', // actionView ProductController
    
    'catalog' => 'catalog/index',  //actionIndex CatalogController
    
    'category/([0-9]+)/page-([0-9]+)' => 'catalog/category/$1/$2', //actionCategory CatalogController
    'category/([0-9]+)' => 'catalog/category/$1', //actionCategory CatalogController
    
    'cart/add/([0-9]+)' => 'cart/add/$1', //actionAdd CartController
    'cart/delete/([0-9]+)' => 'cart/delete/$1', //actionAdd CartController
    
    'cart/addAjax/([0-9]+)' => 'cart/addAjax/$1', 
    'cart' => 'cart/index', //actionIndex
    'cart/checkout' => 'cart/checkout',
    
    'user/register' => 'user/register',
    'user/login' => 'user/login',
    
    'cabinet/edit' => 'cabinet/edit',
    'cabinet' => 'cabinet/index',
    
    'contacts' => 'site/contact',   
    '' => 'site/index',    // actionIdex SiteController
    
);