<?php

class UserController {

    public function actionRegister() {

        $name = '';
        $email = '';
        $password = '';

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::ChekName($name)) {
                $errors [] = 'Name must contain at 2 symbols';
            }

            if (!User::ChekEmail($email)) {
                $errors [] = 'Incorrect Email adress';
            }

            if (!User::ChekPassword($password)) {
                $errors [] = 'Incorrect Password';
            }

            if (User::ChekEmailExists($email)) {
                $errors [] = 'This Email is already in use';
            }

            if ($errors == false) {
                $result = User::register($name, $email, $password);
            }
        }

        require_once(ROOT . '/views/user/register.php');

        return true;
    }

    public function actionLogin() {

        $email = '';
        $password = '';

        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;


            if (!User::ChekEmail($email)) {
                $errors [] = 'Incorrect Email adress';
            }

            if (!User::ChekPassword($password)) {
                $errors [] = 'Incorrect Password';
            }

            // Проверка на существование пользователя
            $userId = User::ChekUserData($email, $password);

            if ($userId == false) {

                $errors [] = 'Неправильные данные для входа на сайт';
            } else {
                // если пользователь есть записываем сессию
                User::auth($userId);

                //Перенаправляем польщователя в кабинет
                header("Location: /cabinet/");
                
            }
        }

        require_once(ROOT . '/views/user/login.php');

        return true;
    }
    
    public function actionLogout(){

        unset($_SESSION['user']);
        header("Location: /");
    }
            

}
