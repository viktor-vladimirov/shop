<?php

include_once ROOT . '/models/Category.php';
include_once ROOT . '/models/Product.php';

class SiteController {

    public function actionIndex() {

        $categories = array();
        $categories = Category::getCategoriesList();
        
        //список последних товаров
        $lastProducts = array();
        $lastProducts = Product::getLastestProducts(6);
        
        //список товаров для слайдера
        $sliderProducts = Product::getRecommendedProducts();

        require(ROOT . '/views/site/index.php');

        return true;
    }

    public function actionContact() {

        $userEmail = '';
        $userText = '';
        $result = false;

        if (isset($_POST['submit'])) {

            $userEmail = $_POST['userEmail'];
            $userText = $_POST['userText'];

            $errors = false;

            if (!User::ChekEmail($userEmail)) {
                $errors [] = 'Неправильный Email';
            }

            if ($errors == false) {

                $mail = 'scoud@mail.ru';
                $subject = 'Тема письма';
                $message = "Текст: {$userText}. От {$userEmail}";
                $result = mail($mail, $subject, $message);
                $result = true;
                                
            }
        }
        
        require_once(ROOT.'/views/site/contact.php');
        
        return true;
    }

}
