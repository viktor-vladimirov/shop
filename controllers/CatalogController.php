<?php



class CatalogController {

    public function actionIndex() {

        $categories = array();
        $categories = Category::getCategoriesList();

        $lastProducts = array();
        $lastProducts = Product::getLastestProducts(10);

        require_once(ROOT . '/views/catalog/index.php');

        return true;
    }

    public function actionCategory($categoryId, $page = 1 ) {
        

        $categories = array();
        $categories = Category::getCategoriesList();

        $categoryProducts = array();
        $categoryProducts = Product::getProductListByCategory($categoryId, $page);
        
        // выводим количество продуктов в конкретнойкатегории
        $total = Product::getTotalProductsInCategory($categoryId);
        
        //Создаем объект Pagination постраничная навигация
        $pagination = new Pagination($total, $page, Product::SHOW_BY_DEFAULT, 'page-');
        

        require_once(ROOT . '/views/catalog/category.php');

        return true;
    }

}
