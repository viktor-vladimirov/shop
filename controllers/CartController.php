<?php

class CartController {

    public function actionAdd($id) {

        //добавляем товар в корзину
        Cart::addProduct($id);

        //возвращаем пользователя на страницу
        $referrer = $_SERVER['HTTP_REFERER'];
        header("Location: $referrer");
    }
    
    public function actionDelete($id){
        
        Cart::cartDeleteProduct($id);
                  
        header("Location: /cart/");
    }

    public function actionAddAjax($id) {

        echo Cart::addProduct($id);
        return true;
    }

    public function actionIndex() {

        $categories = array();
        $categories = Category::getCategoriesList();

        $productsInCart = false;

        //получение данных из корзины
        $productsInCart = Cart::getProducts();

        if ($productsInCart) {

            //получаем полнуюю информацию о товарах
            $productsIds = array_keys($productsInCart);
            $products = Product::getProductsByIds($productsIds);

            //Получаем общую стоимость товара
            $totalPrice = Cart::getTotalPrice($products);
        }

        require_once(ROOT . '/views/cart/index.php');

        return true;
    }

    public function actionCheckout() {

        //список категорий левого меню
        $categories = array();
        $categories = Category::getCategoriesList();

        //статус успешного оформления заказа
        $result = false;

        //проверка отправленна ли форма
        if (isset($_POST['submit'])) {

            //форма отправленна ? да
            //считываем данные формы
            $userName = $_POST['userName'];
            $userPhone = $_POST['userPhone'];
            $userComment = $_POST['userComment'];

            //валидация полей
            $errorrs = false;
            
            if (!User::ChekName($userName))
                $errorrs [] = 'Неправильное имя';
            if (!User::ChekPhone($userPhone))
                $errorrs [] = 'Неправильный номер телефона';

            //форма отправленна корректно ?
            if ($errorrs == false) {
                //если оформленна корректно
                //сохраняем заказ в базе данных
                //собираем инвормацию озаказе
                $productInCart = Cart::getProducts();
                if (User::isGuest()) {
                    $userId = false;
                } else {
                    $userId = User::cheklogged();
                }

                //Сохраняем заказ в бд
                $result = Order::save($userName, $userPhone, $userComment, $userId, $productInCart);

                //оповещаем администратора о новом заказе
                if ($result) {

                    $adminEmail = 'scoud@mail.ru';
                    $message = 'site1/admin/orders';
                    $subject = 'Новый заказ';

                    mail($adminEmail, $subject, $message);

                    //очистка формы
                    Cart::clear();
                }
            } else {
                
                // в корзине есть товары ? да
                //итоги: общая стоимость , количество товара
                $productInCart = Cart::getProducts();
                $productIds = array_keys($productInCart);
                $products = Product::getProductsByIds($productIds);
                $totalPrice = Cart::getTotalPrice($products);
                $totalQuantity = Cart::countItems();
                
            }
        } else {

            //форма не отправленна
            //получаем данные из корзины
            $productInCart = Cart::getProducts();

            //есть в корзине товары ?
            if ($productInCart == false) {
                //если нет 
                //Отправляем пользователя на глувную

                header("Location: /");
            } else {

                // в корзине есть товары ? да
                //итоги: общая стоимость , количество товара
                $productIds = array_keys($productInCart);
                $products = Product::getProductsByIds($productIds);
                $totalPrice = Cart::getTotalPrice($products);
                $totalQuantity = Cart::countItems();

                $userName = false;
                $userPhone = false;
                $userComment = false;

                //Пользователь зврегестрирован ?
                if (User::isGuest()) {
                    //нет
                    //значения для формы пустые
                } else {
                    //да ваторитезирован
                    //получаем информацию о пользователе из бд по id

                    $userId = User::cheklogged();
                    $user = User::getUserById($userId);
                    //подставляем в форму
                    $userName = $user['name'];
                }
            }
        }

        require_once(ROOT . '/views/cart/checkout.php');

        return true;
    }

}
