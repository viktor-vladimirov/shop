<?php

class CabinetController {

    public function actionIndex() {

        //получаем id
        $userId = User::ChekLogged();

        //получаем инфо из БД
        $user = User::getUserById($userId);

        require_once(ROOT . '/views/cabinet/index.php');

        return true;
    }

    public function actionRegister() {

        $name = '';
        $email = '';
        $password = '';

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::ChekName($name)) {
                $errors [] = 'Name must contain at 2 symbols';
            }

            if (!User::ChekEmail($email)) {
                $errors [] = 'Incorrect Email adress';
            }

            if (!User::ChekPassword($password)) {
                $errors [] = 'Incorrect Password';
            }

            if (User::ChekEmailExists($email)) {
                $errors [] = 'This Email is already in use';
            }

            if ($errors == false) {
                $result = User::register($name, $email, $password);
            }
        }

        require_once(ROOT . '/views/user/register.php');

        return true;
    }

    public function actionEdit() {

        //получаем id
        $userId = User::ChekLogged();

        //получаем инфо из БД
        $user = User::getUserById($userId);

        $name = $user['name'];
        $password = $user['password'];

        $result = false;

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $password = $_POST['password'];

            $errors = false;


            if (!User::ChekName($name)) {
                $errors [] = 'Name must contain at 2 symbols';
            }

            if (!User::ChekPassword($password)) {
                $errors [] = 'Incorrect Password';
            }
            
            if($errors == false){
                $result = User::edit($userId, $name, $password);
            }
        }
        
        require_once(ROOT.'/views/cabinet/edit.php');
        
        return true;
    }

}
