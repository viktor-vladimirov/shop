<?php

class Router {

    private $routes;

    public function __construct() {
        #1 создаем путь который записываем в переменную routesPath
        #2 присваеем свойству  $routes массив который храниться в routesPath
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath);
    }

    private function getURI() {
        #1 получаем строку запроса 
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run() {
        #1
        $uri = $this->getURI();

        #2 проверяем наличие маршрута в $routes
        foreach ($this->routes as $uriPattern => $path) {

            #3 сравниваем $uriPattern и $path
            if (preg_match("~^$uriPattern$~", $uri)) {
       
        
             #получаем внутренний путь из внешнего
             $internalRoute = preg_replace("~^$uriPattern$~", $path, $uri);
             
   
             # определяем controller, action и параметры
                $segments = explode('/', $internalRoute);
          
                $controllerName = array_shift($segments) . 'Controller';
                $controllerName = ucfirst($controllerName);

                $actionName = 'action' . ucfirst(array_shift($segments));
                
                $parameters = $segments;
              
                
                

                #4 подключить файл класса-контреллера
                $controllerFile = ROOT . '/controllers/' .
                        $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }



                #5 создаем обьект
                $controllerObject = new $controllerName;
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                if ($result != null) {
                    break;
                }
            }
        }
    }

}
