<?php

class Cart {

    public static function addProduct($id) {

        $id = intval($id);

        //пустой массив для товара
        $productInCart = array();

        //если в сессии есть товары , то считываем и ложим в массив
        if (isset($_SESSION['products'])) {
            $productInCart = $_SESSION['products'];
        }

        //есть ли товар в кoрзине , но был добавлен еще раз, увеличиваем количество
        if (array_key_exists($id, $productInCart)) {
            $productInCart[$id] ++;
        } else {
            $productInCart[$id] = 1;
        }

        $_SESSION['products'] = $productInCart;


        return self::countItems();
    }

    public static function countItems() {

        if (isset($_SESSION['products'])) {
            $count = 0;
            foreach ($_SESSION['products'] as $id => $quantity) {
                $count = $count + $quantity;
            }
            return $count;
        } else {
            return 0;
        }
    }

    public static function getProducts() {

        if (isset($_SESSION['products'])) {
            return $_SESSION['products'];
        }
        return false;
    }

    public static function getTotalPrice($products) {

        $productInCart = self::getProducts();

        $total = 0;

        if ($productInCart) {
            foreach ($products as $item) {
                $total += $item['price'] * $productInCart[$item['id']];
            }
        }

        return $total;
    }

    public static function clear() {

        if (isset($_SESSION['products'])) {
            unset($_SESSION['products']);
        }
    }

    public static function cartDeleteProduct($id) {
        
        $productsInCart = self::getProducts();
        
        unset($productsInCart[$id]);
        
        $_SESSION['products'] = $productsInCart;
/*
        $id = intval($id);

        //пустой массив для товара
        $productInCart = array();

        //если в сессии есть товары , то считываем и ложим в массив
        if (isset($_SESSION['products'])) {
            $productInCart = $_SESSION['products'];
        }

        //есть ли товар в кoрзине , удаляем его по id из сессии
        if (array_key_exists($id, $productInCart)) {
            unset($productInCart[$id]);
        }
        
        $_SESSION['products'] = $productInCart;
 * 
 */
    }

}
