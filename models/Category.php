<?php


class Category {
    //return an array of catigories
    
    public static function getCategoriesList(){
        
        $db = Db::getConnection();
        
        $categoryList = array();
        
        $result = $db->query('SELECT id, name FROM category' 
           . ' ORDER BY sort_order' );
        
        $i = 0;
        while ($row = $result->fetch()) {
            $categoryList[$i]['id'] = $row['id'];
            $categoryList[$i]['name'] = $row['name'];
            $i++;
        }
        
        return $categoryList;
    }
    
}
